/*
*(La clase Factura) Cree una clase llamada Factura, que una ferretería podría utilizar para
representar una factura para un artículo vendido en la tienda. Una Factura debe incluir cuatro 
piezas de información como variables de instancia: un número de pieza (tipo String), la 
descripción de la pieza (tipo String), la cantidad de artículos de ese tipo que se van a 
comprar (tipo int) y el precio por artículo (double). Su clase debe tener un constructor que 
inicialice las cuatro variables de instancia. Proporcione un método establecer y uno obtener 
para cada variable de instancia. Además, proporcione un método llamado obtenerMontoFactura, 
que calcule el monto de la factura (es decir, que multiplique la cantidad por el precio por 
artículo) y después lo devuelva como un valor double. Si la cantidad no es positiva, debe 
establecerse en 0. Si el precio por artículo no es positivo, debe establecerse en 0.0. Escriba
una aplicación de prueba llamada PruebaFactura, que demuestre las capacidades de la clase Factura.
*/
package deber1pooherencia3ciclo;
public class Factura {
    //Atríbutos
    public String NPieza;
    public int cantidadArt;
    public double precio;
    double montoFactura;
    //Constructor
    public Factura(String NPieza, int cantidadArt, double precio) {
        this.NPieza = NPieza;
        this.cantidadArt = cantidadArt;
        this.precio = precio;
    }
    public String getNPieza() {
        return NPieza;
    }
    public void setNPieza(String NPieza) {
        this.NPieza = NPieza;
    }
    public int getCantidadArt() {
        return cantidadArt;
    }
    public void setCantidadArt(int cantidadArt) {
        this.cantidadArt = cantidadArt;
    }
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public void obtenerMontoFactura(){
        if (this.getCantidadArt() < 0) 
            this.cantidadArt=0; 
        if (this.getPrecio() < 0) 
            this.precio=0.0;
        montoFactura = this.getCantidadArt()*this.getPrecio(); 
        //return montoFactura;
    }
    @Override
    public String toString(){
        obtenerMontoFactura();
        return " "+montoFactura;
    }
}
class PruebaFactura{
    public static void main(String[] args) {
        Factura factura1 = new Factura("Carro", 1, 3);
        System.out.println(factura1);
        Factura factura2 = new Factura("Carro", -5, 3);
        System.out.println(factura2);
    }
}
